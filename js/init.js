$(document).ready(function(){
	$('.stars-rating').addRating({max:10});
    $('select').material_select();
	$('.datepicker').pickadate({
		selectYears:20,
		format: 'yyyy-mm-dd',
		formatSubmit: 'yyyy-mm-dd',
		hiddenName: true
	});
	Materialize.updateTextFields();
})

function autofill_episode() {
	var id = document.getElementById('id').value;
	window.location.href = window.location.href + "&autofill=" + id;
}

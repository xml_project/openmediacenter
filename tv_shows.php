<?php

require_once 'vendor/autoload.php';

include 'config.php';
include 'functions.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($loader);

$vars = array();
$vars['shows'] = list_dirs($config['TV Shows']);
$vars['shows_poster'] = array();

foreach ($vars['shows'] as $key => $show) {
    $poster = $config['TV Shows'] . '/' . $show . '/poster.jpg';
    if (is_file($poster)) {
        $vars['shows_poster'][$key] = $poster;
    }
}

if (isset($_GET['show'])) {
    $vars['show'] = $_GET['show'];
    $vars['seasons'] = array();
    if(isset($_GET['season'])) {
        $vars['season'] = $_GET['season'];
        $vars['episodes'] = array();
        if(isset($_GET['episode'])) {
            $vars['episode'] = $_GET['episode'];
        }
    }
}

$show_path = '';
$season_path = '';
$episode_path ='';

$infos = array();


if (isset($_GET['show'])) {
    $show_path = $config['TV Shows'] . '/' . $_GET['show'];
    $xml = $show_path . '/tvshow.xml';

    if (!isset($_GET['episode']) && isset($_POST['action']) && isset($_GET['edited'])) {
        $values = array(
            'Title' => strip_tags($_POST['title']),
            'Id' => strip_tags($_POST['id']),
            'ContentRating' => strip_tags($_POST['content_rating']),
            'Rating' => strip_tags($_POST['rating']),
            'Year' => strip_tags($_POST['year']),
            'Runtime' => strip_tags($_POST['runtime']),
            'Genres' => array(),
            'Plot' => strip_tags($_POST['plot']),
            'Persons' => array(
                'Person' => array()
            )
        );

        $values['Genres']['Genre'] = array();
        $genres = explode('|', strip_tags($_POST['genres']));
        foreach ($genres as $g) {
            $values['Genres']['Genre'][] = $g;
        }

        $values['Persons']['Person'] = array();
        if ($values['Persons']['Person'] != '||') {
            $persons = explode('|', strip_tags($_POST['persons']));
            foreach ($persons as $p) {
                $values['Persons']['Person'][] = $p;
            }
            foreach ($values['Persons']['Person'] as $key => $value) {
                if ($value) {
                    $tmp = explode(';', $value);
                    $values['Persons']['Person'][$key] = array();
                    $values['Persons']['Person'][$key]['Name'] = $tmp[0];
                    $values['Persons']['Person'][$key]['Type'] = $tmp[1];
                    $values['Persons']['Person'][$key]['Role'] = $tmp[2];
                }
            }
        }

        $str = $twig->render('tvshow.xml',array('infos' => $values));

        save_xml($xml, $str);
    }

    $infos['ContentRating'] = set_tv_cr(null);

    if(file_exists($xml)) {
        $isValid = validate_xml($xml, 'dtd/series.dtd', 'Series');
        $vars['show_info'] = load_xml($xml);
        $vars['show_info']['Plot'] = fix_empty_array($vars['show_info']['Plot']);
        if (!isset($_GET['episode']) && isset($_GET['edit'])) {
            $infos = $vars['show_info'];
            $infos['ContentRating'] = set_tv_cr($infos['ContentRating']);
            $genres = $infos['Genres']['Genre'];
            if (!(is_array($genres) or ($genres instanceof Traversable))) {
                $genres = array($genres);
            }
            $infos['Genres'] = implode('|',$genres);
            foreach ($infos['Persons']['Person'] as $key => $value) {
                $infos['Persons']['Person'][$key] = implode(';',$value);
            }
            $infos['Persons'] = implode('|',$infos['Persons']['Person']);
            $infos['Persons'] = $infos['Persons'] == '||' ? '' : $infos['Persons'];
        }

        if (!(is_array($vars['show_info']['Genres']['Genre']) or ($vars['show_info']['Genres']['Genre'] instanceof Traversable))) {
            $vars['show_info']['Genres']['Genre'] = array($vars['show_info']['Genres']['Genre']);
        }
    }
    else {
        $vars['show_info']['Title'] = $_GET['show'];
    }

    if (!isset($_GET['episode']) && isset($_GET['edit']) && isset($_GET['autofill'])) {
        $infos = tvdbtoomc_show(load_xml('http://thetvdb.com/api/'.$config['api_key'].'/series/'.$_GET['autofill'].'/fr.xml'));
        $infos['ContentRating'] = set_tv_cr($infos['ContentRating']);
        $infos['Persons'] = '';
    }

    foreach (list_dirs($show_path) as $season) {
        if (preg_match('/\d+$/', $season, $matches)) {
            $vars['seasons'][$matches[0]] = $season;
        }
    }

    foreach ($vars['seasons'] as $key => $season) {
        $poster = $show_path . '/' . $season . '/poster.jpg';
        if (is_file($poster)) {
            $vars['seasons_poster'][$key] = $poster;
        }
    }

}

if (isset($_GET['season'])) {
    $season_path = $show_path . '/' . $vars['seasons'][$_GET['season']];
    foreach (list_files($season_path) as $episode) {
        if (preg_match('/s\d+e(\d+)\.(mp4|mkv)$/', $episode, $matches)) {
            $vars['episodes'][$matches[1]] = $episode;
        }
    }

    foreach ($vars['episodes'] as $key => $episode) {
        $thumbnail = create_thumbnail($season_path . '/' . $episode);
        if (is_file($thumbnail)) {
            $vars['ep_thumb'][$key] = $thumbnail;
        }
    }
}


if (isset($_GET['episode'])) {
    $episode_path = $season_path . '/' . $vars['episodes'][$_GET['episode']];
    $vars['episode_url'] = $episode_path;

    $xml = preg_replace('/([^.]+)$/', 'xml', $episode_path);

    if (isset($_POST['action']) && isset($_GET['edited'])) {
        $values = array(
            'Title' => strip_tags($_POST['title']),
            'Id' => strip_tags($_POST['id']),
            'Season' => strip_tags($_POST['season']),
            'Episode' => strip_tags($_POST['episode']),
            'ContentRating' => strip_tags($_POST['content_rating']),
            'Rating' => strip_tags($_POST['rating']),
            'Runtime' => strip_tags($_POST['runtime']),
            'Aired' => strip_tags($_POST['aired']),
            'Genres' => array(),
            'Plot' => strip_tags($_POST['plot']),
            'Persons' => array(
                'Person' => array()
            )
        );

        $values['Genres']['Genre'] = array();
        $genres = explode('|', strip_tags($_POST['genres']));
        foreach ($genres as $g) {
            $values['Genres']['Genre'][] = $g;
        }

        $values['Persons']['Person'] = array();
        if ($values['Persons']['Person'] != '||') {
            $persons = explode('|', strip_tags($_POST['persons']));
            foreach ($persons as $p) {
                $values['Persons']['Person'][] = $p;
            }
            foreach ($values['Persons']['Person'] as $key => $value) {
                if ($value) {
                    $tmp = explode(';', $value);
                    $values['Persons']['Person'][$key] = array();
                    $values['Persons']['Person'][$key]['Name'] = $tmp[0];
                    $values['Persons']['Person'][$key]['Type'] = $tmp[1];
                    $values['Persons']['Person'][$key]['Role'] = $tmp[2];
                }
            }
        }

        $str = $twig->render('episode.xml',array('infos' => $values));

        save_xml($xml, $str);
    }

    $infos['ContentRating'] = set_tv_cr(null);

    if(file_exists($xml)) {
        $isValid = validate_xml($xml, 'dtd/episode.dtd', 'EpisodeDetails');
        $vars['episode_info'] = load_xml($xml);
        $vars['episode_info']['Plot'] = fix_empty_array($vars['episode_info']['Plot']);

        if (isset($_GET['edit'])) {
            $infos = $vars['episode_info'];
            $infos['ContentRating'] = set_tv_cr($infos['ContentRating']);
            $genres = $infos['Genres']['Genre'];
            if (!(is_array($genres) or ($genres instanceof Traversable))) {
                $genres = array($genres);
            }
            $infos['Genres'] = implode('|',$genres);
            foreach ($infos['Persons']['Person'] as $key => $value) {
                $infos['Persons']['Person'][$key] = implode(';',$value);
            }
            $infos['Persons'] = implode('|',$infos['Persons']['Person']);
            $infos['Persons'] = $infos['Persons'] == '||' ? '' : $infos['Persons'];
        }
    }

    if (isset($_GET['autofill'])) {
        $infos = tvdbtoomc_ep(load_xml('http://thetvdb.com/api/'.$config['api_key'].'/episodes/'.$_GET['autofill'].'/fr.xml'));
        $infos['ContentRating'] = set_tv_cr($infos['ContentRating']);
        $infos['Genres'] = '';
        $infos['Persons'] = '';
    }
}



if (isset($_GET['episode'])) {
    $player_title = $vars['show'] . ' - Saison ' . $vars['season'] . ' - Episode ' . $vars['episode'];

    if (isset($_GET['edit'])) {
        $page = 'tv_shows.php?show='.$vars['show'].'&season='.$vars['season'].'&episode='.$vars['episode'];
        echo $twig->render('form_episode.html', array('infos' => $infos, 'page' => $page, 'path' => $episode_path));
    }
    else {
        echo $twig->render('episode.html', array('vars' => $vars, 'title' => $player_title));
    }
}
else if (isset($_GET['season'])) {
    echo $twig->render('season.html', array('vars' => $vars));
}
else if (isset($_GET['show'])) {
    if (isset($_GET['edit'])) {
        $page = 'tv_shows.php?show='.$vars['show'];
        echo $twig->render('form_tvshow.html', array('infos' => $infos, 'page' => $page, 'path' => $show_path));
    }
    else {
        echo $twig->render('tv_show.html', array('vars' => $vars));
    }
}
else {
    echo $twig->render('tv_shows.html', array('vars' => $vars));
}

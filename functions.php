<?php

function list_dirs($path) {
    $dirs = array();
    foreach (glob($path.'/*', GLOB_ONLYDIR) as $dir) {
        $dirs[] = basename($dir);
    }
    natcasesort($dirs);
    return $dirs;
}

function list_files($path) {
    $files = array();
    foreach (glob($path.'/*') as $file) {
        if(is_file($file)) {
            $files[] = basename($file);
        }
    }

    natcasesort($files);
    return $files;
}

function load_xml($path) {
    $xml = simplexml_load_file($path, "SimpleXMLElement", LIBXML_NOCDATA);
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);

    return $array;
}

function save_xml($path, $xml) {
    $exists = file_exists($path);
    $fp = fopen($path,'w');
    fwrite($fp,$xml);
    fclose($fp);
    if (!$exists) {
        chmod($path, 0777);
    }
}

function tvdbtoomc_ep($tvdb) {
    $tvdb = $tvdb['Episode'];
    $omc = array(
        'Title' => $tvdb['EpisodeName'],
        'Id' => $tvdb['id'],
        'Season' => $tvdb['SeasonNumber'],
        'Episode' => $tvdb['EpisodeNumber'],
        'ContentRating' => '',
        'Rating' => round($tvdb['Rating']),
        'Runtime' => '',
        'Aired' => $tvdb['FirstAired'],
        'Genres' => array(),
        'Plot' => $tvdb['Overview'],
        'Persons' => array(
            'Person' => array()
        )
    );

    return $omc;
}

function tvdbtoomc_show($tvdb) {
    $tvdb = $tvdb['Series'];
    $omc = array(
        'Title' => $tvdb['SeriesName'],
        'Id' => $tvdb['id'],
        'ContentRating' => $tvdb['ContentRating'],
        'Rating' => round($tvdb['Rating']),
        'Runtime' => $tvdb['Runtime'],
        'Year' => explode('-',$tvdb['FirstAired'])[0],
        'Genres' => trim($tvdb['Genre'],'|'),
        'Plot' => $tvdb['Overview'],
        'Persons' => array(
            'Person' => array()
        )
    );

    return $omc;
}

function validate_xml($xml, $dtd, $root) {
    $old = new DOMDocument;
    $old->load($xml);

    $creator = new DOMImplementation;
    $doctype = $creator->createDocumentType($root, null, $dtd);
    $new = $creator->createDocument(null, null, $doctype);
    $new->encoding = "utf-8";

    $oldNode = $old->getElementsByTagName($root)->item(0);
    $newNode = $new->importNode($oldNode, true);
    $new->appendChild($newNode);

    $new->validate();
}

function create_thumbnail($file) {
    $thumbnail = 'thumbnails/'.preg_replace('/([^.]+)$/', 'png', $file);
    $folder = preg_replace('/([^\/]+)$/', '', $thumbnail);
    if (!file_exists($thumbnail)) {
        shell_exec('mkdir -p "'.$folder.'"');
        shell_exec('ffmpeg -ss 00:00:5.000 -i "'.$file.'" -vframes 1 -filter:v scale="280:-1" "'.$thumbnail.'" 2>&1');
    }
    return $thumbnail;
}

function set_tv_cr($c) {
    $content_rating = array('TV-Y','TV-Y7','TV-G','TV-PG','TV-14','TV-MA');
    if (isset($c) && in_array($c,$content_rating)) {
        $content_rating = array_fill_keys($content_rating, ' ');
        $content_rating[$c] = ' selected';
    }
    else {
        $content_rating = array_fill_keys($content_rating, ' ');
    }
    return $content_rating;
}

function set_movie_cr($c) {
    $content_rating = array('G','PG','PG-13','R','NC-17');
    if (isset($c) && in_array($c,$content_rating)) {
        $content_rating = array_fill_keys($content_rating, ' ');
        $content_rating[$c] = ' selected';
    }
    else {
        $content_rating = array_fill_keys($content_rating, ' ');
    }
    return $content_rating;
}

function fix_empty_array($a) {
    if (is_array($a) or ($a instanceof Traversable)) {
        $a = '';
    }
    return $a;
}

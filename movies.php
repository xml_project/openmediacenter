<?php

require_once 'vendor/autoload.php';

include 'config.php';
include 'functions.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($loader);

$vars = array();
$vars['movies'] = list_dirs($config['Movies']);
$vars['movies_poster'] = array();

foreach ($vars['movies'] as $key => $movie) {
    $poster = $config['Movies'] . '/' . $movie . '/poster.jpg';
    if (is_file($poster)) {
        $vars['movies_poster'][$movie] = $poster;
    }
}

if (isset($_GET['movie'])) {
    $vars['movie'] = $_GET['movie'];
}

$movie_path = '';

$infos = array();


if (isset($_GET['movie'])) {
    $movie_path = $config['Movies'] . '/' . $_GET['movie'];

    foreach (list_files($movie_path) as $file) {
        if (preg_match('/.+\.(mp4|mkv)$/', $file)) {
            $vars['movies'][$_GET['movie']] = $file;
        }
    }

    $xml = preg_replace('/([^.]+)$/', 'xml', $movie_path . '/' . $vars['movies'][$_GET['movie']]);

    if (isset($_POST['action']) && isset($_GET['edited'])) {
        $values = array(
            'Title' => strip_tags($_POST['title']),
            'Id' => strip_tags($_POST['id']),
            'ContentRating' => strip_tags($_POST['content_rating']),
            'Rating' => strip_tags($_POST['rating']),
            'Runtime' => strip_tags($_POST['runtime']),
            'Year' => strip_tags($_POST['year']),
            'Genres' => array(),
            'Plot' => strip_tags($_POST['plot']),
            'Persons' => array(
                'Person' => array()
            )
        );

        $values['Genres']['Genre'] = array();
        $genres = explode('|', strip_tags($_POST['genres']));
        foreach ($genres as $g) {
            $values['Genres']['Genre'][] = $g;
        }

        $values['Persons']['Person'] = array();
        if ($values['Persons']['Person'] != '||') {
            $persons = explode('|', strip_tags($_POST['persons']));
            foreach ($persons as $p) {
                $values['Persons']['Person'][] = $p;
            }
            foreach ($values['Persons']['Person'] as $key => $value) {
                if ($value) {
                    $tmp = explode(';', $value);
                    $values['Persons']['Person'][$key] = array();
                    $values['Persons']['Person'][$key]['Name'] = $tmp[0];
                    $values['Persons']['Person'][$key]['Type'] = $tmp[1];
                    $values['Persons']['Person'][$key]['Role'] = $tmp[2];
                }
            }
        }

        $str = $twig->render('movie.xml',array('infos' => $values));

        save_xml($xml, $str);
    }

    $infos['ContentRating'] = set_movie_cr(null);

    if(file_exists($xml)) {
        $isValid = validate_xml($xml, 'dtd/movie.dtd', 'Movie');
        $vars['movie_info'] = load_xml($xml);
        $vars['movie_info']['Plot'] = fix_empty_array($vars['movie_info']['Plot']);

        if (isset($_GET['edit'])) {
            $infos = $vars['movie_info'];
            $infos['ContentRating'] = set_movie_cr($infos['ContentRating']);
            $genres = $infos['Genres']['Genre'];
            if (!(is_array($genres) or ($genres instanceof Traversable))) {
                $genres = array($genres);
            }
            $infos['Genres'] = implode('|',$genres);
            foreach ($infos['Persons']['Person'] as $key => $value) {
                $infos['Persons']['Person'][$key] = implode(';',$value);
            }
            $infos['Persons'] = implode('|',$infos['Persons']['Person']);
            $infos['Persons'] = $infos['Persons'] == '||' ? '' : $infos['Persons'];
        }
    }
}



if (isset($_GET['movie'])) {
    $player_title = $vars['movie'];
    $vars['movie_url'] = $movie_path . '/' . $vars['movies'][$_GET['movie']];

    if (isset($_GET['edit'])) {
        $page = 'movies.php?movie='.$vars['movie'];
        echo $twig->render('form_movie.html', array('infos' => $infos, 'page' => $page, 'path' => $movie_path . '/' . $vars['movies'][$_GET['movie']]));
    }
    else {
        echo $twig->render('movie.html', array('vars' => $vars, 'title' => $player_title));
    }
}
else {
    echo $twig->render('movies.html', array('vars' => $vars));
}
